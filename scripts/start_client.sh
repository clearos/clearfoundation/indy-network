#!/bin/bash

set -e

if [ ! -z "$IPS" ]; then
    echo von_generate_transactions -s "$IPS"
    von_generate_transactions -s "$IPS"
elif [ ! -z "$IP" ]; then
    echo von_generate_transactions -i "$IP"
    von_generate_transactions -i "$IP"
else
    echo von_generate_transactions
    von_generate_transactions
fi

mkdir -p .indy_client/pool/clear
cp /home/indy/ledger/clear/pool_transactions_genesis .indy_client/pool/clear/clear.txn

indy-cli
